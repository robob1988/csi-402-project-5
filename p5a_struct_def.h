/* Robert Lowell RL659896 */
/* rlowell@albany.edu     */
/* Project 5    5/6/2013  */
/* p5a_struct_def.h        */

#ifndef P5A_STRUCT_DEF_H
#define P5A_STRUCT_DEF_H

#include "p5a_constants.h"

/* File Info  Structure */
typedef struct infonode {
  char fileLength;
  char fileName[MAX_FILENAME_LENGTH]; 
  unsigned int fileSize;
  struct infonode *next;
} *INFONODE;

#endif

