/* Robert Lowell RL659896 */
/* rlowell@albany.edu     */
/* Project 5   5/6/2013   */
/* p5a_main.c             */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "p5a_constants.h"
#include "p5a_struct_def.h"
#include "p5a_prototypes.h"

/* Main Program Function */
int main(int argc, char *argv[]){
  /* Take in the command line arguments. Determine whether we are */
  /* creating an archive or extracting an archive. If we are      */
  /* creating an archive, loop through a struct to store info     */
  /* about each of the files listed in the command line. Then,    */
  /* store the correct header to the new archive file, and then   */
  /* store all the information from our struct into the archive.  */
  /* Finally, run our printLoop that will open a file, and write  */
  /* it to the archive file serially until all are complete.      */
  /* If we are extracting an archive file, retrieve the info for  */
  /* each of the files in the archive and store it into a struct. */
  /* Once we have our information, loop through the structs and   */
  /* created a file with the correct name and data from the       */
  /* archive file.                                                */


  /* File variables. */
  FILE *archiveFile;

  /* Archive information variables. */
  char fileCount = 0;
  int i = 0;

  /* File information struct */
  INFONODE infoList = NULL, tempNode = NULL, newNode = NULL;

  /* Stat struct */
  struct stat fileStat;

  /* Check to see if we  meet minimum command line args */
  if(argc < MIN_ARGS){
    fprintf(stderr, "Program usage: p5a [-c,-x] archive [optional: infile1 infile2 ..]\n"); fflush(stderr);
    exit(1);
  }
  
  /* Check to see if we are under the maximum number of args */
  if(argc > MAX_ARGS){
    fprintf(stderr, "Too many arguments: p5a [-c, -x] archive infile1 to infile255\n"); fflush(stderr);
    exit(1);
  }

  /* Determine what the flag is, either -c or -x */
  if(strcmp(argv[FLAG_ARG], "-c") == 0){
    /* Flag is a create flag. Open new archive file. */
    
    /* Open archive */
    if((archiveFile = fopen(argv[ARCHIVE_FILE_ARG], "w")) == NULL){
      /* Open Failed */
      fprintf(stderr, "File: %s could not be opened for writing.\n", argv[ARCHIVE_FILE_ARG]); fflush(stderr);
      exit(1);
    }

    /* Determine how many files that are going to be added to the archive. */
    fileCount = argc - MIN_ARGS;

    /* Store this number in our archive. */
    fwrite((const void *)&fileCount, 1, sizeof(fileCount), archiveFile);

      /* Loop through the file names and store the file information */
    /* in the information struct.                                 */
    for(i = IN_FILE_START_ARG; i < argc; i++){
      /* Get the stats for the file */
      if(stat(argv[i], &fileStat) < 0){
	/* An error occured accesing the file information. */
	fprintf(stderr, "File: %s's information could not be accessed.\n", argv[i]); fflush(stderr);
	exit(1);
      }
      
      /* Store the file information in the infoList. */
      /* Check to see if the list is empty. */
      if(infoList == NULL){
	/* The list is empty. Create a new node. */
	if((infoList = (struct infonode *)malloc(sizeof(struct infonode))) == NULL){
	  fprintf(stderr, "infoNode allocation failed.\n"); fflush(stderr);
	  exit(1);
	}
	/* Store the information in the new node. */
	/* Store file name length. */
	infoList->fileLength = strlen(argv[i]);
	
	/* Store file name. */
	strcpy(infoList->fileName, argv[i]);

	/* Store file size. */
	infoList->fileSize = fileStat.st_size;
      }
      else{
	/* List is not null. Create a new node and add it to the end of the list. */
        tempNode = infoList;

	/* Move to the end of the list. */
	for(; tempNode->next != NULL; tempNode = tempNode->next);

	if((newNode = (struct infonode *)malloc(sizeof(struct infonode))) == NULL){
          fprintf(stderr, "infoNode allocation failed.\n"); fflush(stderr);
          exit(1);
        }

        /* Store the information in the new node. */
        /* Store file name length. */
        newNode->fileLength = strlen(argv[i]);

        /* Store file name. */
        strcpy(newNode->fileName, argv[i]);

        /* Store file size. */
        newNode->fileSize = fileStat.st_size;
	
	/* Put newNode at the end of the list. */
	tempNode->next = newNode;
      }
    }

    /* Pass our archiveFile and infoList to archivePrint function. */
    archivePrint(archiveFile, infoList);

    /* Close the archiveFile. */
    if(fclose(archiveFile) == EOF){
      fprintf(stderr, "Error closing file %s\n", argv[ARCHIVE_FILE_ARG]); fflush(stderr);
    }
  }
  else if(strcmp(argv[FLAG_ARG], "-x") == 0){
    /* Flag is extract flag. Call our extract function. */

    /* Open archiveFile */
    if((archiveFile = fopen(argv[ARCHIVE_FILE_ARG], "r")) == NULL){
      /* Open Failed */
      fprintf(stderr, "File: %s could not be opened for writing.\n", argv[ARCHIVE_FILE_ARG]); fflush(stderr);
      exit(1);
    }
    
    /* Call archiveExtract to get the files out of the archive. */
    archiveExtract(archiveFile);
  
    /* Close the archiveFile. */
    if(fclose(archiveFile) == EOF){
      fprintf(stderr, "Error closing file %s\n", argv[ARCHIVE_FILE_ARG]); fflush(stderr);
    }
  }
  else{
    /* Flag doesn't exist. */
    fprintf(stderr, "Flag: %s is incorrect. Use -c or -x.\n", argv[FLAG_ARG]); fflush(stderr);
    exit(1);
  }

  return (0);

}
