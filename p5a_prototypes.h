/* Robert Lowell RL659896 */
/* rlowell@albany.edu     */
/* Project 5    5/6/2013  */
/* p5a_prototypes.h       */

#ifndef P5A_PROTOTYPES_H
#define P5A_PROTOTYPES_H

void archivePrint(FILE *, INFONODE);
void archiveExtract(FILE *);

#endif
