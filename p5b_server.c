/* Robert Lowell RL659896 */
/* rlowell@albany.edu     */
/* Project 5   5/6/2013   */
/* p5b_server.c           */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <errno.h>
#include <fcntl.h>

#include "p5b_constants.h"

/* Private prototypes. */
void loadDatabase(FILE *);
void writeDatabase(FILE *);
int addcComm(char *);
int drpcComm(char *);
int wdrwComm(char *);
int tcreComm(char *);
int newcComm(char *);
int cschComm(char *);
int ccreComm(char *);
char* gschComm(char *);
int gcreComm(char *);

typedef struct student {
  char studentName[STUDENT_NAME_LENGTH];
  int numCourses;
  int courseList[MAX_STUDENT_COURSES];
} *STUDENT;

typedef struct course {
  int courseID;
  int numCredits;
  char schedule[SCHEDULE_LENGTH];
} *COURSE;

/* Global database struct variables. */
STUDENT studentList[MAX_STUDENTS];
COURSE courseList[MAX_COURSES];
int numStudents = 0, numCourses = 0;

/* Main Server Function */
int main(int argc, char *argv[]){
  /* Read in the server file. Create the FIFOs for reading / writing */
  /* to the client. Fork the clien process and process the commands. */
  /* On "exit" command write database file.                          */

  /* File pointers. */
  FILE *initDB, *finalDB;

  /* Name of FIFOs Command and Response, respectively. */
  char *comFifo = "COM_FIFO";
  char *resFifo = "RES_FIFO";
  
  /* Name of client program. */
  char *clientProg = "p5b_client";
  
  int comFD, resFD;

  /* Message buffer. */
  char comBuf[MSGSIZE+1];
  char resBuf[MSGSIZE+1];
  char *tempSchedule;
  int result = 0;

  /* Token */
  char *token;

  /* pid variables. */
  pid_t child;
    
  /* Check if we have the correct amount of arguements */
  if(argc != NUM_ARGS){
    fprintf(stderr, "Arguement usage: initdb finaldb cmdfile logfile\n"); fflush(stderr);
    exit(1);
  }

  /* Open initdb file to get database information */
  if((initDB = fopen(argv[INIT_DB_ARG], "r")) == NULL){
    /* Open Failed */
    fprintf(stderr, "File: %s could not be opened for reading.\n", argv[INIT_DB_ARG]); fflush(stderr);
    exit(1);
  }

  /* Open initdb file to get database information */
  if((finalDB = fopen(argv[FINAL_DB_ARG], "w")) == NULL){
    /* Open Failed */
    fprintf(stderr, "File: %s could not be opened for reading.\n", argv[FINAL_DB_ARG]); fflush(stderr);
    exit(1);
  }

  /* Store database into memory. */
  loadDatabase(initDB);  

  /* Make our FIFOs, check if they exist */
  if (mkfifo(comFifo, FIFO_MODE) == -1) {
    if (errno != EEXIST) {
      fprintf(stderr, "Server: Couldn't create %s fifo.\n", comFifo); exit(1);
    }
  }

  if (mkfifo(resFifo, FIFO_MODE) == -1) {
    if (errno != EEXIST) {
      fprintf(stderr, "Server: Couldn't create %s fifo.\n", resFifo); exit(1);
    }
  }
  
  /* Open comFifo for reading, and resFifo for writing. */
  if ((comFD = open(comFifo, O_RDONLY | O_NONBLOCK)) == -1) {
    fprintf(stderr, "Server: %s FIFO open failed.\n", comFifo); exit(1);
  }

  /* Fork the client process. */
  if ((child = fork()) == 0) { /* Child process. */
    /* Execute client process. */
    execvp(clientProg, argv); 

    /* If the child process reaches this point, then */
    /* execvp must have failed. */
    fprintf(stderr, "Child process could not do execlp.\n");
    exit(1);
  }
  else { /* Parent process. */
    if (child == (pid_t)(-1)) {
      fprintf(stderr, "Fork failed.\n"); exit(1);
    }
    else {      
      if ((resFD = open(resFifo, O_WRONLY)) == -1) {
	fprintf(stderr, "Server: %s FIFO open failed.\n", resFifo); exit(1);
      }
      /* Client process is running. Listen for commands. */
      fprintf(stdout, "Working."); fflush(stdout);
      for(;;){
	fprintf(stdout, "."); fflush(stdout);
	switch(read(comFD, comBuf, MSGSIZE)) {
	case -1: /* Make sure that pipe is empty. */
	  if (errno == EAGAIN) {
	    sleep(SLEEP_TIME); break;
	  }
	  else { /* Reading from pipe failed. */
	    fprintf(stderr, "Server: Couldn't read from pipe.\n"); exit(1);
	  }
	case 0: /* Pipe has been closed. */
	  /* Exit the program. */
	  /* Write to the final database program. */
	  writeDatabase(finalDB);

	  /* Close the FIFOs. */
	  if(close(comFD) == -1){
	    fprintf(stderr, "Error closing %s\n", comFifo); fflush(stderr);
	  }
          if(close(resFD) == -1){
            fprintf(stderr, "Error closing %s\n", resFifo); fflush(stderr);
          }

	  /* Close the files. */
	  if(fclose(initDB) == EOF){
	    fprintf(stderr, "Error closing file %s\n", argv[INIT_DB_ARG]); fflush(stderr);
	  }
	  if(fclose(finalDB) == EOF){
	    fprintf(stderr, "Error closing file %s\n", argv[FINAL_DB_ARG]); fflush(stderr);
	  }

	  fprintf(stdout, "Done.\n"); fflush(stdout);

	  /* Exit */
	  return(0);
	default:
	  /* Recieved a command. Parse it and apply it to the database. */	  
	  token = strtok(comBuf, " \n");

	  /* Find out which command we have. */
	  if(strcmp(token, "addc") == 0){
	    result = addcComm(token);
	    
	    /* Write our result to the respond fifo to be written to the log. */
	    sprintf(resBuf, "%d", result);
	    write(resFD, resBuf, MSGSIZE);
	  }
	  else if(strcmp(token, "drpc") == 0){
	    result = drpcComm(token);

            /* Write our result to the respond fifo to be written to the log. */
            sprintf(resBuf, "%d", result);
            write(resFD, resBuf, MSGSIZE);
	  }
	  else if(strcmp(token, "wdrw") == 0){
            result = wdrwComm(token);

            /* Write our result to the respond fifo to be written to the log. */
            sprintf(resBuf, "%d", result);
            write(resFD, resBuf, MSGSIZE);
          }
	  else if(strcmp(token, "tcre") == 0){
            result = tcreComm(token);

            /* Check to see if the command was a success. */
	    if(result != -1){
	      /* It was a success. Send the success and the credits. */
	      sprintf(resBuf, "1   %d", result);

	      write(resFD, resBuf, MSGSIZE);
	    }
	    else{
	      /* It was a failure, send the failure. */
	      sprintf(resBuf, "0   %d", result);
              write(resFD, resBuf, MSGSIZE);
	    }
          }
	  else if(strcmp(token, "newc") == 0){
            result = newcComm(token);

            /* Write our result to the respond fifo to be written to the log. */
            sprintf(resBuf, "%d", result);
            write(resFD, resBuf, MSGSIZE);
          }
	  else if(strcmp(token, "csch") == 0){
            result = cschComm(token);

            /* Write our result to the respond fifo to be written to the log. */
            sprintf(resBuf, "%d", result);
            write(resFD, resBuf, MSGSIZE);
          }
	  else if(strcmp(token, "ccre") == 0){
            result = ccreComm(token);

            /* Write our result to the respond fifo to be written to the log. */
            sprintf(resBuf, "%d", result);
            write(resFD, resBuf, MSGSIZE);
          }
	  else if(strcmp(token, "gsch") == 0){
            tempSchedule = gschComm(token);

	    /* Check to see if it was an error or not. */
	    if(strcmp(tempSchedule, "Error") == 0){
	      /* It was unsuccessful, send the error. */
	      sprintf(resBuf, "0   %s", tempSchedule);
	      write(resFD, resBuf, MSGSIZE);
	    }
	    else{
	      /* It was successful, print the schedule. */
	      sprintf(resBuf, "1   %s", tempSchedule);
	      write(resFD, resBuf, MSGSIZE);
	    }
          }
	  else if(strcmp(token, "gcre") == 0){
            result = gcreComm(token);

	    /* Check to see if the command succeeded */
	    if(result != -1){
	      /* Command succeeded, send it to the pipe. */
	      sprintf(resBuf, "1   %d", result);
              write(resFD, resBuf, MSGSIZE);
	    }
	    else{
	      /* Command failed, send error to the pipe. */
	      sprintf(resBuf, "0   %d", result);
              write(resFD, resBuf, MSGSIZE);
	    }
          }
	  else{
	    /* Invalid command, send an error the to client. */
	    result = 0;
            /* Write our result to the respond fifo to be written to the log. */
            sprintf(resBuf, "%d", result);
            write(resFD, resBuf, MSGSIZE);
	  }
	}
      } 
    }
  }
}

void loadDatabase(FILE *initDB){
  /* Parse through the initial database and store it's information in memory. */
  
  char buffer[MAX_LINE_LENGTH];
  char *token;
  
  int i = 0, j = 0;

  /* Initialize our student and course arrays. */
  for(i = 0; i < MAX_STUDENTS; i++){
    studentList[i] = NULL;
    courseList[i] = NULL;
  }

  /* Read in how many lines of students there are. */
  fgets(buffer, MAX_LINE_LENGTH, initDB);

  /* Convert the string to an int. */
  numStudents = atoi(buffer);

  /* Loop through the number of students in the database gathering information. */
  for(i = 0; i < numStudents; i++){
    fgets(buffer, MAX_LINE_LENGTH, initDB);
    
    /* Tokenize the buffer to get the student's name. */
    token = strtok(buffer, " \n");
    
    /* Allocate memory for the student entry into our student list. */
    if((studentList[i] = (struct student *)malloc(sizeof(struct student))) == NULL){
      fprintf(stderr, "Student allocation failed.\n"); fflush(stderr);
      exit(1);
    }

    /* Initialize student's course list array. */
    for(j = 0; j < MAX_STUDENT_COURSES; j++){
      studentList[i]->courseList[j] = -1;
    }

    /* Store the student's name in the student array. */
    strcpy(studentList[i]->studentName, token);

    /* Tokenize how many courses they are taking. */
    token = strtok(NULL, " \n");
    
    studentList[i]->numCourses = atoi(token);
    
    /* Loop through this number of courses and store them in the student's course list. */
    for(j = 0; j < studentList[i]->numCourses; j++){
      token = strtok(NULL, " \n");

      studentList[i]->courseList[j] = atoi(token);
    }
  }

  /* We have all the students stored. Store course information. */
  fgets(buffer, MAX_LINE_LENGTH, initDB);

  numCourses = atoi(buffer);

  /* Loop through the number of courses in the database gathering information. */
  for(i = 0; i < numCourses; i++){
    fgets(buffer, MAX_LINE_LENGTH, initDB);

    /* Tokenize the buffer to get the course id. */
    token = strtok(buffer, " \n");

    /* Allocate memory for the course entry into our course list. */
    if((courseList[i] = (struct course *)malloc(sizeof(struct course))) == NULL){
      fprintf(stderr, "Course allocation failed.\n"); fflush(stderr);
      exit(1);
    }

    /* Store course ID in the course list. */
    courseList[i]->courseID = atoi(token);

    /* Tokenize the number of credits. */
    token = strtok(NULL, " \n");

    /* Store the number of credits. */
    courseList[i]->numCredits = atoi(token);

    /* Tokenize the course schedule. */
    token = strtok(NULL, " \n");
    
    /* Store the course schedule. */
    strcpy(courseList[i]->schedule, token);
  }
}


void writeDatabase(FILE *finalDB){
  /* Write the final in-memory database to the finalDB file. */
  int i = 0, j = 0;


  /* Write the number of students. */
  fprintf(finalDB, "%d\n", numStudents); fflush(finalDB);
  
  /* Loop through the students and print their information. */
  for(i = 0; i < numStudents; i++){
    fprintf(finalDB,"%-15s ", studentList[i]->studentName); fflush(finalDB);
    fprintf(finalDB,"%-3d ", studentList[i]->numCourses); fflush(finalDB);
    for(j = 0; j < studentList[i]->numCourses; j++){
      fprintf(finalDB,"%-5d ", studentList[i]->courseList[j]); fflush(finalDB);
    }  
    fprintf(finalDB,"\n"); fflush(finalDB);
  }

  /* Loop through the courses and print their information. */
  fprintf(finalDB, "%d\n", numCourses); fflush(finalDB);
  for(i = 0; i < numCourses; i++){
    fprintf(finalDB, "%-5d ", courseList[i]->courseID); fflush(finalDB);
    fprintf(finalDB, "%-3d ", courseList[i]->numCredits); fflush(finalDB);
    fprintf(finalDB, "%s\n", courseList[i]->schedule); fflush(finalDB);
  }
}


int addcComm(char *token){
  /* Check through our course list to see if the course exists. */
  /* If it does exists, search through our student list to see  */
  /* if the student exists. If so, add the course to the student*/
  /* list if they do not have 10 courses. If they do not exist, */
  /* add them to the student list if they will not exceed 100   */
  /* students.                                                  */

  /* Temp Variables. */
  char tempStudent[STUDENT_NAME_LENGTH];
  int tempCourse = 0;
  int i = 0, j = 0, k = 0;

  /* Take in the next two parameters and store them in temp variables. */
  token = strtok(NULL, " \n");

  /* We have the student name, store this in tempStr */
  strcpy(tempStudent, token);

  /* Tokenize our course ID value. */
  token = strtok(NULL, " \n");

  tempCourse = atoi(token);

  /* Check to see if the course exists. */
  for(i = 0; i < numCourses; i++){
    if(courseList[i]->courseID == tempCourse){
      /* This courses exists. See if the student exists. */
      for(j = 0; j < numStudents; j++){
	if(strcmp(studentList[j]->studentName, tempStudent) == 0){
	  /* We have a student match. Check to see if they have less */
	  /* than 10 courses currently listed.                       */
	  if(studentList[j]->numCourses < MAX_STUDENT_COURSES){
	    /* Student has less than maximum amount of courses. */
	    /* See if the student already has the course listed. */
	    for(k = 0; k < studentList[j]->numCourses; k++){
	      if(studentList[j]->courseList[k] == tempCourse){
		/* The student already has the course listed. Return error. */
		return 0;
	      }
	    }
	    /* Student doesn't already have the course listed. Add the course*/
	    studentList[j]->courseList[studentList[j]->numCourses] = tempCourse;

	    /* Increase the amount of courses this student has. */
	    studentList[j]->numCourses++;
	    
	    /* Command Success.*/
	    return 1;
	  }
	  else{
	    /* Student already has a maximum of 10 courses. */
	    /* Return a failed command.                     */
	    return 0;
	  }
	}
      }
      /* No students were found, check if we are at student maximum. */
      /* If not, add the student to the end of the list.             */
      if(numStudents < MAX_STUDENTS){
	/* We have room to add another student. Add them to the list. */
	/* Allocate memory for the student entry into our student list. */
	if((studentList[numStudents] = (struct student *)malloc(sizeof(struct student))) == NULL){
	  fprintf(stderr, "Student allocation failed.\n"); fflush(stderr);
	  exit(1);
	}
	/* Add student's name to the list. */
	strcpy(studentList[numStudents]->studentName, tempStudent);

	/* Set their number of coureses to 1. */
	studentList[numStudents]->numCourses = 1;

	/* Add the course to their course list. */
	studentList[numStudents]->courseList[0] = tempCourse;

	/* Increase student count. */
	numStudents++;

	/* Command success. */
	return 1;
      }
    }/* end of course search if. */
  }/* End of for loop. */
  /* Course was not listed. Return an error. */
  return 0;
}


int drpcComm(char *token){
  /* Drop a course for the provided student. If the student */
  /* isn't enrolled in the course, report an error.         */

  /* Temp Variables. */
  char tempStudent[STUDENT_NAME_LENGTH];
  int tempCourse = 0;
  int i = 0, j = 0;

  /* Tokenize the student name. */
  token = strtok(NULL, " \n");

  /* Store student name in temp variable. */
  strcpy(tempStudent, token);

  /* Tokenize the course ID. */
  token = strtok(NULL, " \n");

  /* Store the course ID in temp variable. */
  tempCourse = atoi(token);

  /* Search to see if the student exists in the database. */
  for(i = 0; i < numStudents; i++){
    if(strcmp(studentList[i]->studentName, tempStudent) == 0){
      /* We have found the student. */
      /* Search the student's courses to find a match. */
      for(j = 0; j < studentList[i]->numCourses; j++){
	if(studentList[i]->courseList[j] == tempCourse){
	  /* We have found the course. Remove it from the */
	  /* course list. Subtract 1 from the number of   */
	  /* courses and compact the course list array.   */
	  
	  /* Check to see if the course we are removing is the */
	  /* last course in the list.                          */
	  if(j == (studentList[i]->numCourses - 1)){
	    /* We are removing the last course in the list. */
	    /* Just subtract 1 from the number of courses.  */
	    studentList[i]->numCourses--;
	    
	    /* Return a success. */
	    return 1;
	  }
	  else{
	    /* Loop through the remaining number of courses and */
	    /* compact the course list.                         */
	    for(; j < (studentList[i]->numCourses - 1); j++){
	      /* Move next course into this course spot, removing */
	      /* the course to be deleted.                        */
	      studentList[i]->courseList[j] = studentList[i]->courseList[j+1];
	    }
	     
	    /* Reduce course count by 1. */
	    studentList[i]->numCourses--;

	    /* Return a success. */
	    return 1;
	  }
    	}/* End of if */
      }/* End of j for loop */
      /* No course match was found. Return an error. */
      return 0;
    }
  }/* End of i for loop. */


  /* No student match was found, return error. */
  return 0;
}


int wdrwComm(char *token){
  /* Take in the student name, and see if it exists. */
  /* If it exists, remove all courses from the course*/
  /* list for the student.                           */

  /* Temp Variables. */
  char tempStudent[STUDENT_NAME_LENGTH];
  int i = 0;

  /* Tokenize the student name. */
  token = strtok(NULL, " \n");
  
  /* Store the student name in the temp variable. */
  strcpy(tempStudent, token);

  /* Search for the student in the database. */
  for(i = 0; i < numStudents; i++){
    if(strcmp(studentList[i]->studentName, tempStudent) == 0){
      /* We have a student match check if student has any courses. */
      if(studentList[i]->numCourses > 0){
	/* Student is enrolled. Withdraw all courses*/
   	studentList[i]->numCourses = 0;
    
	/* Return a succes. */
	return 1;
      }
      else{
	/* Student is not enrolled, return a failure. */
	return 0;
      }
    }
  }
  
  /* No student matches were found, return a failure. */
  return 0;
}


int tcreComm(char *token){
  /* Search the database for the provided student name. */
  /* If the name exists, add up the number of credits   */
  /* the student is taking and return it. Otherwise,    */
  /* return an error of -1.                             */

  /* Temp Variables. */
  char tempStudent[STUDENT_NAME_LENGTH];
  int numCredits = 0;
  int i = 0, j = 0, k = 0;

  /* Tokenize student name. */
  token = strtok(NULL, " \n");

  /* Store the student name. */
  strcpy(tempStudent, token);
  
  /* Search for the student in the database. */
  for(i = 0; i < numStudents; i++){
    if(strcmp(studentList[i]->studentName, tempStudent) == 0){
      /* We have a student match, get sum of all credits. */
      for(j = 0; j < studentList[i]->numCourses; j++){
	/* Search for matching course information. */
	for(k = 0; k < numCourses; k++){
	  if(courseList[k]->courseID == studentList[i]->courseList[j]){
	    /* We have found a course match, add credits. */
	    numCredits += courseList[k]->numCredits;
	    break;
	  }
	}
      }
      /* Return number of credits. */
      return numCredits;
    }
  }

  /* No student match was found, return error. */
  return -1;
}


int newcComm(char *token){
  /* Add a new course to the database. First, check if the number */
  /* of credits provided is positive (more than 0). and that the  */
  /* course doesn't already exist. Also check if adding the course*/
  /* will bring the total number of courses over 100. If all these*/
  /* conditions are satisfied, add the new course.               */

  /* Temp Variables. */
  int tempCourseID = 0, tempCredits = 0;
  char tempSchedule[SCHEDULE_LENGTH];
  int i = 0;

  /* Parse the course information from the command. */
  token = strtok(NULL, " \n");

  /* Store the course ID. */
  tempCourseID = atoi(token);

  /* Tokenize the number of credits for the course. */
  token = strtok(NULL, " \n");

  /* Store the number of credits for the course. */
  tempCredits = atoi(token);

  /* Tokenize the schedule for the course. */
  token = strtok(NULL, " \n");

  /* Store the course schedule. */
  strncpy(tempSchedule, token, SCHEDULE_LENGTH);

  /* Check to see if the credits for the course are over 0. */
  if(tempCredits > 0){
    /* The credits are positive and above zero. See if adding */
    /* this course will bring us over the 100 course limit.   */
    if(numCourses == 100){
      /* Adding a new course will bring us over the 100 course limit. */
      /* Return an error. */
      return 0;
    }
    
    /* There are less than 100 courses. Check to see if the course */
    /* already exists in the database.                             */
    for(i = 0; i < numCourses; i++){
      /* Search for a matching course. */
      if(courseList[i]->courseID == tempCourseID){
	/* The course already exists, report an error. */
	return 0;
      }
    }
    
    /* The course doesn't exists, add the new course to the end of */
    /* the course list. Allocate memory for the new course.        */
    if((courseList[numCourses] = (struct course *)malloc(sizeof(struct course))) == NULL){
      fprintf(stderr, "Course allocation failed.\n"); fflush(stderr);
      exit(1);
    }

    courseList[numCourses]->courseID = tempCourseID;
    courseList[numCourses]->numCredits = tempCredits;
    strcpy(courseList[numCourses]->schedule, tempSchedule);
    
    numCourses++;

    /* Return a success. */
    return 1;
  }     
       
  /* Credits are less than 1, return an error. */
  return 0;
}


int cschComm(char *token){
  /* Check to see if the course provided exists. If so, */
  /* replace the existing schedule with the updated one.*/

  char tempSchedule[SCHEDULE_LENGTH];
  int i = 0, tempCourseID = 0;

  /* Tokenize our course ID */
  token = strtok(NULL, " \n");

  /* Store the course ID. */
  tempCourseID = atoi(token);

  /* Tokenize the course schedule. */
  token = strtok(NULL, " \n");

  /* Store the course schedule. */
  strcpy(tempSchedule, token);

  /* See if the course exsist. */
  for(i = 0; i < numCourses; i++){
    /* Search for a matching course. */
    if(courseList[i]->courseID == tempCourseID){
      /* The course exists, update the schedule. */
      strcpy(courseList[i]->schedule, tempSchedule);
      
      /* Return success. */
      return 1;
    }
  }
  /* The course doesn't exist, return an error. */
  return 0;
}


int ccreComm(char *token){
  /* Check to see if the credits recieved are positive. */
  /* If so, check to see if the course exists. If it the*/
  /* course exists, change its credit value to the input*/

  int tempCourseID = 0, tempCredits = 0;
  int i = 0;

  /* Tokenize the course ID */
  token = strtok(NULL, " \n");

  /* Store course id in temp variable. */
  tempCourseID = atoi(token);

  /* tokenize credit amount. */
  token = strtok(NULL, " \n");

  /* Store credit amount. */
  tempCredits = atoi(token);

  /* Check to see if the credit amount is greater than 0. */
  if(tempCredits > 0){
    /* Credit amount is positive, check if the course exists. */
    for(i = 0; i < numCourses; i++){
      /* Search for a matching course. */
      if(courseList[i]->courseID == tempCourseID){
	/* The course exists, update the crecits. */
	courseList[i]->numCredits = tempCredits;

	/* Return success. */
	return 1;
      }
    }
    /* Course didn't exist, return error. */
    return 0;
  }
  
  /* Credit amount was 0 or less, error. */
  return 0;
}


char* gschComm(char *token){
  /* Check to see if the course exists. If so, retrieve */
  /* the course schedule information and return it.     */

  int tempCourseID = 0;
  int i = 0;
  char tempSchedule[SCHEDULE_LENGTH];
  char *result;
  char *error = "Error";

  /* Tokenize the course ID */
  token = strtok(NULL, " \n");

  /* Store course id. */
  tempCourseID = atoi(token);

  /* Check to see if the course exists. */  
  for(i = 0; i < numCourses; i++){
    /* Search for a matching course. */
    if(courseList[i]->courseID == tempCourseID){
      /* The course exists, return the schedule. */
      strcpy(tempSchedule, courseList[i]->schedule);
      result = tempSchedule;

      /* Return success. */
      return result;
    }
  }
  /* Course doesn't exist, return error. */
  return error;
}


int gcreComm(char *token){
  /* Check to see if the course exists. If it does, */
  /* return the number of credits for the course. If*/
  /* the course doesn't exist, return -1.           */

  int tempCourseID = 0;
  int i = 0;

  /* Tokenize the course ID */
  token = strtok(NULL, " \n");

  /* Store course id. */
  tempCourseID = atoi(token);

  /* Check to see if the course exists. */
  for(i = 0; i < numCourses; i++){
    /* Search for a matching course. */
    if(courseList[i]->courseID == tempCourseID){
      /* The course exists, return number of credits. */
      return courseList[i]->numCredits;
    }
  }
  
  /* Course doesn't exist, return error. */
  return -1;
}





