/* Robert Lowell RL659896 */
/* rlowell@albany.edu     */
/* Project 5    5/6/2013  */
/* p5a_constants.h        */

#ifndef P5A_CONSTANTS_H
#define P5A_CONSTANTS_H

#define FLAG_ARG 1 
#define ARCHIVE_FILE_ARG 2
#define IN_FILE_START_ARG 3
#define MIN_ARGS 3
#define MAX_ARGS 258
#define MAX_FILENAME_LENGTH 256

#endif
