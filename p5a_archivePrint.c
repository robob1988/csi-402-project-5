/* Robert Lowell RL659896 */
/* rlowell@albany.edu     */
/* Project 5    5/6/2013  */
/* p5a_archivePrint.c     */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "p5a_constants.h"
#include "p5a_struct_def.h"

/* Private prototypes. */
void dataPrint(FILE *, char[], int);


void archivePrint(FILE *archiveFile, INFONODE infoList){
  /* Loop through our infoList struct and print it's info to */
  /* the archive file. Then print the information from the   */
  /* files serially into the archive file.                   */

  INFONODE tempNode = NULL;
 
  /* Set tempNode to head of infoList */
  tempNode = infoList;


  /* Loop through our infoList and print file information */
  /* to the archiveFile.                                  */
  for(; tempNode != NULL; tempNode = tempNode->next){
    
    /* Write the file name length. */
    fwrite((const void *)&tempNode->fileLength, 1, sizeof(tempNode->fileLength), archiveFile);

    /* Write the file name. */
    fwrite(tempNode->fileName, 1, tempNode->fileLength, archiveFile);

    /* Write the file size in bytes. */
    fwrite((const void *)&tempNode->fileSize, 1, sizeof(tempNode->fileSize), archiveFile);

  }

  /* File information has been printed. Now print the data from the files */
  /* in order to the archive file.                                        */
  tempNode = infoList;

  /* Loop through our list of files getting the file names. */
  for(; tempNode != NULL; tempNode = tempNode->next){
    /* Get the file name, and call dataPrint. */
    dataPrint(archiveFile, tempNode->fileName, tempNode->fileLength);
  }
} 

void dataPrint(FILE *archiveFile, char fileStr[MAX_FILENAME_LENGTH], int fileLength){
  /* Print the data from the file given to the archive. */

  FILE *inFile = NULL;
  char buffer = NULL, fileName[fileLength];

  /* Copy fileStr into fileName of appropriate length. */
  strcpy(fileName, fileStr);

  /* Open the appropriate file with the given filename. */
  if((inFile = fopen(fileName, "r")) == NULL){
    /* Open Failed */
    fprintf(stderr, "File: %s could not be opened for reading.\n", fileName); fflush(stderr);
    exit(1);
  }

  /* Read each byte in the input file, and print it to the archive file. */
  while(fread(&buffer, sizeof(buffer), 1, inFile) == 1){
    /* Write the byte to the archive file. */
    fwrite((const void *)&buffer, 1, sizeof(buffer), archiveFile);
  }

  /* Close the file we have read. */
  if(fclose(inFile) == EOF){
    fprintf(stderr, "Error closing file %s\n", fileName);
  }
}
