/* Robert Lowell RL659896 */
/* rlowell@albany.edu     */
/* Project 5   5/6/2013   */
/* p5b_client.h           */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <errno.h>
#include <fcntl.h>

#include "p5b_constants.h"

/* Main Client Function */
int main(int argc, char *argv[]){
  /* Open the command and log files passed from the server.      */
  /* Parse the command file and send instructions to the server. */

  /* File pointers. */
  FILE *comFile, *logFile;

  /* File read buffer. */
  char buffer[MAX_LINE_LENGTH+1];

  /* Name of FIFOs Command and Response, respectively. */
  char *comFifo = "COM_FIFO";
  char *resFifo = "RES_FIFO";

  /* FIFO file descriptors. */
  int comFD, resFD;

  /* Message buffer. */
  char comBuf[MSGSIZE+1];
  char resBuf[MSGSIZE+1];

  /* Counters. */
  int commandLine = 0;
  int responseFlag = 0;

  /* tempCommand */
  char *tempCommand;

  /* Open command and log files. */
  if((comFile = fopen(argv[CMD_FILE_ARG], "r")) == NULL){
    /* Open Failed */
    fprintf(stderr, "File: %s could not be opened for reading.\n", argv[CMD_FILE_ARG]); fflush(stderr);
    exit(1);
  }

  if((logFile = fopen(argv[LOG_FILE_ARG], "w")) == NULL){
    /* Open Failed */
    fprintf(stderr, "File: %s could not be opened for reading.\n", argv[LOG_FILE_ARG]); fflush(stderr);
    exit(1);
  }

  /* Open FIFOs for reading / writing. */
  if((comFD = open(comFifo, O_RDWR)) == -1) {
    fprintf(stderr, "Client: %s FIFO open failed.\n", comFifo); exit(1);
  }
  if((resFD = open(resFifo, O_RDONLY)) == -1) {
    fprintf(stderr, "Client: %s FIFO open failed.\n", resFifo); exit(1);
  }
  
  /* Parse command file line by line. */
  while(fgets(buffer, MAX_LINE_LENGTH, comFile) != NULL){
    /* Read in each line of the command file and send it to the server. */
    /* Write our command to the server, then wait for a response.       */
    strcpy(comBuf, buffer);

    if(write(comFD, comBuf, MSGSIZE) == -1){
      fprintf(stderr, "Client: Writing to %s FIFO failed.\n", comFifo); exit(1);
    }

    tempCommand = strtok(buffer, " \n");
    /* Write log number and command to log file. */
    fprintf(logFile, "%2d   %s   ", commandLine, tempCommand);

    /* Listen for responses from the server. */
    for(responseFlag = 0; responseFlag == 0;){
      switch(read(resFD, resBuf, MSGSIZE)) {
      case -1: /* Make sure that pipe is empty. */
	if (errno == EAGAIN) {
	  sleep(SLEEP_TIME); break;
	}
	else { /* Reading from pipe failed. */
	  fprintf(stderr, "Client: Couldn't read from pipe.\n"); exit(1);
	}
      case 0: /* Pipe has been closed. */
	responseFlag = 1;
      default:
	/* Recieved a response. Write it to the log. */
	fprintf(logFile, "%s\n", resBuf);
	responseFlag = 1;
	commandLine++;
	break;
      }
    }
  }

  /* We are done using the FIFOs, close them. */
  if(close(comFD) == -1){
    fprintf(stderr, "Closing %s failed.\n", comFifo); fflush(stdout);
  }
  if(close(resFD) == -1){
    fprintf(stderr, "Closing %s failed.\n", resFifo); fflush(stdout);
  }

  /* Close command and log files. */
  if(fclose(comFile) == EOF){
    fprintf(stderr, "Error closing file %s\n", argv[CMD_FILE_ARG]);
  }
  if(fclose(logFile) == EOF){
    fprintf(stderr, "Error closing file %s\n", argv[LOG_FILE_ARG]);
  }

  return(0);
}
