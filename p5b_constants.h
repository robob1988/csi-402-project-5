/* Robert Lowell RL659896 */
/* rlowell@albany.edu     */
/* Project 5   5/6/2013   */
/* p5b_constants.h        */

#ifndef P5B_CONSTANTS_H
#define P5B_CONSTANTS_H

#define NUM_ARGS 5
#define INIT_DB_ARG 1
#define FINAL_DB_ARG 2
#define CMD_FILE_ARG 3
#define LOG_FILE_ARG 4

#define STUDENT_NAME_LENGTH 31
#define SCHEDULE_LENGTH 21
#define MAX_LINE_LENGTH 63

#define MAX_STUDENTS 100
#define MAX_COURSES 100
#define MAX_STUDENT_COURSES 10

#define MSGSIZE 63
#define FIFO_MODE 0666
#define SLEEP_TIME 1


#endif
