/* Robert Lowell RL659896 */
/* rlowell@albany.edu     */
/* Project 5    5/6/2013  */
/* p5a_archiveExtract.c   */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "p5a_constants.h"
#include "p5a_struct_def.h"

/* Private Prototypes */
void filePrint(FILE *, char[], int, int);


void archiveExtract(FILE *archiveFile){
  /* Extract the files from the archive file. */

  /* File information variables. */
  INFONODE infoList = NULL, tempNode = NULL, newNode = NULL;
  char numFiles = 0, fileLength = 0;
  unsigned int fileSize = 0;
  char fileName[MAX_FILENAME_LENGTH];
  int i = 0;


  /* Read in the number of files that are stored in the archive. */
  if(fread(&numFiles, sizeof(numFiles), 1, archiveFile) != 1){
    /* Read failed. */
    fprintf(stderr, "Failed to read from archive file.\n"); fflush(stderr);
  }

  /* Loop through the number of files that are in the archive  */
  /* storing the file's information in our infoList.           */
  for(i = 0; i < numFiles; i++){
    /* Read in our file name's length. */
    if(fread(&fileLength, sizeof(fileLength), 1, archiveFile) != 1){
      /* Read failed. */
      fprintf(stderr, "Failed to read from archive file.\n"); fflush(stderr);
    }
    /* Check to see if infoList is empty. */
    if(infoList == NULL){
      /* The list is empty. Create a new node. */
      if((infoList = (struct infonode *)malloc(sizeof(struct infonode))) == NULL){
	fprintf(stderr, "infoNode allocation failed.\n"); fflush(stderr);
	exit(1);
      }
      /* Store the information in the new node. */
      /* Store file name length. */
      infoList->fileLength = fileLength;

      /* Read in file name. */
      if(fread(fileName, fileLength, 1, archiveFile) != 1){
	/* Read failed. */
	fprintf(stderr, "Failed to read from archive file.\n"); fflush(stderr);
      }
      /* Store file name. */
      strncpy(infoList->fileName, fileName, fileLength);

      /* Read in file size. */
      if(fread(&fileSize, sizeof(fileSize), 1, archiveFile) != 1){
	/* Read failed. */
	fprintf(stderr, "Failed to read from archive file.\n"); fflush(stderr);
      }
      /* Store file size. */
      infoList->fileSize = fileSize;
    }
    else{
      /* List is not null. Create a new node and add it to the end of the list. */
      tempNode = infoList;

      /* Move to the end of the list. */
      for(; tempNode->next != NULL; tempNode = tempNode->next);

      if((newNode = (struct infonode *)malloc(sizeof(struct infonode))) == NULL){
	fprintf(stderr, "infoNode allocation failed.\n"); fflush(stderr);
	exit(1);
      }

      /* Store the information in the new node. */
      /* Store file name length. */
      newNode->fileLength = fileLength;

      /* Read in file name. */
      if(fread(fileName, fileLength, 1, archiveFile) != 1){
        /* Read failed. */
        fprintf(stderr, "Failed to read from archive file.\n"); fflush(stderr);
      }
      /* Store file name. */
      strncpy(newNode->fileName, fileName, fileLength);

      /* Read in file size. */
      if(fread(&fileSize, sizeof(fileSize), 1, archiveFile) != 1){
        /* Read failed. */
        fprintf(stderr, "Failed to read from archive file.\n"); fflush(stderr);
      }
      /* Store file size. */
      newNode->fileSize = fileSize;

      /* Put newNode at the end of the list. */
      tempNode->next = newNode;
    }
  }

  /* We have the information for all of the files. Print the data from archive */
  /* to the appropriate file names.                                            */
  tempNode = infoList;
  
  for(; tempNode != NULL; tempNode = tempNode->next){    
    filePrint(archiveFile, tempNode->fileName, tempNode->fileLength, tempNode->fileSize);
  }

}


void filePrint(FILE *archiveFile, char fileStr[MAX_FILENAME_LENGTH], int fileLength, int fileSize){

  int i = 0;
  char buffer = NULL, fileName[fileLength];
  FILE *outFile = NULL;

  /* Copy fileStr into fileName of appropriate length. */
  strcpy(fileName, fileStr);
 
  /* Open our file for writing. */
  if((outFile = fopen(fileName, "w")) == NULL){
    /* Open Failed */
    fprintf(stderr, "File: %s could not be opened for writing.\n", fileName); fflush(stderr);
    exit(1);
  }

  /* Read fileSize # of bytes from the archive and print them into the output file. */
  for(i = 0; i < fileSize; i++){
    if(fread(&buffer, sizeof(buffer), 1, archiveFile) == 1){
      /* Write the byte to the output file. */
      fwrite((const void *)&buffer, 1, sizeof(buffer), outFile);
    }
    else{
      /* Read failed. */
      fprintf(stderr, "Reading from the archive file failed.\n"); fflush(stderr);
    }
  }

  /* Close output file. */
  if(fclose(outFile) == EOF){
    fprintf(stderr, "Error closing file %s\n", fileName); fflush(stderr);
  }
}
